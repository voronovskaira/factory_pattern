package pizzamaker;

import myexception.UnknownPizzaTypeException;
import pizza.PizzaType;

public class PizzaMakerFactory {
    private static PizzaMaker pizzaMaker;

    public static PizzaMaker choosePizza(PizzaType pizzaType) {
        if (pizzaType.equals(PizzaType.PEPPERONI))
            pizzaMaker = new PepperoniMaker();
        else if (pizzaType.equals(PizzaType.CHEESE))
            pizzaMaker = new CheeseMaker();
        else if (pizzaType.equals(PizzaType.CLAM))
            pizzaMaker = new ClamMaker();
        else if (pizzaType.equals(PizzaType.VEGGIE))
            pizzaMaker = new VeggieMaker();
        return pizzaMaker;
    }
}
