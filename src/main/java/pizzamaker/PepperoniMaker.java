package pizzamaker;

import pizza.Bakery;
import pizza.Pizza;
import pizzafactory.CreatePizza;
import pizzafactory.PepperoniPizza;

public class PepperoniMaker implements PizzaMaker {
    @Override
    public Pizza createPizza(Bakery bakeryCity) {
        CreatePizza pizzaCreator = new PepperoniPizza();
        Pizza pizza = pizzaCreator.prepare(bakeryCity);
        pizzaCreator.bake();
        pizzaCreator.cut();
        pizzaCreator.box();
        return pizza;
    }
}
