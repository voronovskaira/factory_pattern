package pizzamaker;

import pizza.Bakery;
import pizza.Pizza;
import pizzafactory.CheesePizza;
import pizzafactory.CreatePizza;
import pizzafactory.PepperoniPizza;

public class CheeseMaker implements PizzaMaker{
    @Override
    public Pizza createPizza(Bakery bakeryCity) {
        CreatePizza pizzaCreator = new CheesePizza();
        Pizza pizza = pizzaCreator.prepare(bakeryCity);
        pizzaCreator.bake();
        pizzaCreator.cut();
        pizzaCreator.box();
        return pizza;
    }
}
