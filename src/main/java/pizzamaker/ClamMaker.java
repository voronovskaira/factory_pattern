package pizzamaker;

import pizza.Bakery;
import pizza.Pizza;
import pizzafactory.ClamPizza;
import pizzafactory.CreatePizza;
import pizzafactory.PepperoniPizza;

public class ClamMaker implements PizzaMaker{
    @Override
    public Pizza createPizza(Bakery bakeryCity) {
        CreatePizza pizzaCreator = new ClamPizza();
        Pizza pizza = pizzaCreator.prepare(bakeryCity);
        pizzaCreator.bake();
        pizzaCreator.cut();
        pizzaCreator.box();
        return pizza;
    }
}
