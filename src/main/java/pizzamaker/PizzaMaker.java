package pizzamaker;

import pizza.Bakery;
import pizza.Pizza;

public interface PizzaMaker {
    Pizza createPizza(Bakery bakeryCity);
}
