package pizzamaker;

import pizza.Bakery;
import pizza.Pizza;
import pizzafactory.CreatePizza;
import pizzafactory.PepperoniPizza;
import pizzafactory.VeggiePizza;

public class VeggieMaker implements PizzaMaker{
    @Override
    public Pizza createPizza(Bakery bakeryCity) {
        CreatePizza pizzaCreator = new VeggiePizza();
        Pizza pizza = pizzaCreator.prepare(bakeryCity);
        pizzaCreator.bake();
        pizzaCreator.cut();
        pizzaCreator.box();
        return pizza;
    }
}
