package myexception;

public class UnknownPizzaTypeException extends Exception{
    @Override
    public String getMessage() {
        return "Sorry we have no such pizza, go to another pizzeria";
    }
}
