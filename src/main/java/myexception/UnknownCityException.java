package myexception;

public class UnknownCityException extends Exception{
    @Override
    public String getMessage() {
        return "Sorry, unknown city";
    }
}
