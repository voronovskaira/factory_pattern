package pizza;

public enum Dough {
    THICK, THIN_CRUST;
}
