package pizza;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI;
}
