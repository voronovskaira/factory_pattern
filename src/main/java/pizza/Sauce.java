package pizza;

public enum Sauce {
    MARINARA, PLUM_TOMATO, PESTO;
}
