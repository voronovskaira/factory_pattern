package pizza;

import java.util.ArrayList;
import java.util.List;

public class Pizza {
    private PizzaType name;
    private Dough dough;
    private int price;
    private Sauce sauce;
    private List<Topping> toppingList;

    public Pizza(PizzaType name, Dough dough, int price, Sauce sauce) {
        this.name = name;
        this.dough = dough;
        this.price = price;
        this.sauce = sauce;
        this.toppingList = new ArrayList<>();
    }


    public void addTopping(Topping topping) {
        this.toppingList.add(topping);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name=" + name +
                ", dough=" + dough +
                ", price=" + price +
                ", sauce=" + sauce +
                ", toppingList=" + toppingList +
                '}';
    }
}
