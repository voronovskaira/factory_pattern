package pizza;

public enum Topping {
    PEPPERONI, MUSHROOMS, ONIONS, BACON, EXTRA_CHEESE, OLIVES;

}
