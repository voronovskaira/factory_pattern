import view.ConsoleView;

public class Main {
    public static void main(String[] args) {
        Application app = new Application(new ConsoleView());
        app.runApplication();
    }
}
