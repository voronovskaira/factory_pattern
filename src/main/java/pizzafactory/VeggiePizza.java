package pizzafactory;

import pizza.*;

public class VeggiePizza implements CreatePizza {
    private Pizza pizza;

    @Override
    public Pizza prepare(Bakery bakeryCity) {
        if (bakeryCity.equals(Bakery.LVIV)) {
            pizza = new Pizza(PizzaType.VEGGIE, Dough.THIN_CRUST, 12, Sauce.PESTO);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.ONIONS);
        }else if(bakeryCity.equals(Bakery.KYIV)) {
            pizza = new Pizza(PizzaType.VEGGIE, Dough.THIN_CRUST, 14, Sauce.PESTO);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.ONIONS);
        }else if(bakeryCity.equals(Bakery.DNIPRO)) {
            pizza = new Pizza(PizzaType.VEGGIE, Dough.THIN_CRUST, 15, Sauce.PESTO);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.ONIONS);
            pizza.addTopping(Topping.EXTRA_CHEESE);
            pizza.addTopping(Topping.ONIONS);
        }
        return pizza;
    }
}
