package pizzafactory;

import pizza.*;

public class PepperoniPizza implements CreatePizza {
    private Pizza pizza;

    @Override
    public Pizza prepare(Bakery bakeryCity) {
        if(bakeryCity.equals(Bakery.LVIV)) {
            pizza = new Pizza(PizzaType.PEPPERONI, Dough.THICK, 14, Sauce.MARINARA);
            pizza.addTopping(Topping.PEPPERONI);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.ONIONS);
        }else if(bakeryCity.equals(Bakery.KYIV)){
            pizza = new Pizza(PizzaType.PEPPERONI, Dough.THIN_CRUST, 15, Sauce.PLUM_TOMATO);
            pizza.addTopping(Topping.PEPPERONI);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.ONIONS);
            pizza.addTopping(Topping.OLIVES);
        }else if(bakeryCity.equals(Bakery.DNIPRO)){
            pizza = new Pizza(PizzaType.PEPPERONI, Dough.THIN_CRUST, 13, Sauce.PLUM_TOMATO);
            pizza.addTopping(Topping.PEPPERONI);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.BACON);
        }

        return pizza;
    }
}
