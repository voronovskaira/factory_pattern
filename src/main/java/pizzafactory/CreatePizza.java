package pizzafactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizza.Bakery;
import pizza.Pizza;

public interface CreatePizza {
    Pizza prepare(Bakery bakeryCity);

    Logger LOG = LogManager.getLogger(CreatePizza.class);


    default void bake() {
        LOG.info("Your pizza is baking...Please wait");
        try {
            Thread.sleep(2000);
            LOG.info("Pizza is baked");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    default void cut() {
        LOG.info("Your pizza is cutting...Please wait");
        try {
            Thread.sleep(1000);
            LOG.info("Pizza is cut");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    default void box() {
        LOG.info("Your pizza is ready. Have a nice day!");
    }
}
