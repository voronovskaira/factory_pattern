package pizzafactory;

import pizza.*;

public class ClamPizza implements CreatePizza{
    private Pizza pizza;

    @Override
    public Pizza prepare(Bakery bakeryCity) {
        if(bakeryCity.equals(Bakery.LVIV)) {
            pizza = new Pizza(PizzaType.CLAM, Dough.THIN_CRUST, 11, Sauce.MARINARA);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.ONIONS);
            pizza.addTopping(Topping.OLIVES);
            pizza.addTopping(Topping.BACON);
        }else if(bakeryCity.equals(Bakery.KYIV)){
            pizza = new Pizza(PizzaType.CLAM, Dough.THICK, 12, Sauce.PLUM_TOMATO);
            pizza.addTopping(Topping.PEPPERONI);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.ONIONS);
            pizza.addTopping(Topping.OLIVES);
        }else if(bakeryCity.equals(Bakery.DNIPRO)){
            pizza = new Pizza(PizzaType.CLAM, Dough.THICK, 13, Sauce.PLUM_TOMATO);
            pizza.addTopping(Topping.PEPPERONI);
            pizza.addTopping(Topping.MUSHROOMS);
            pizza.addTopping(Topping.BACON);
            pizza.addTopping(Topping.MUSHROOMS);
        }

        return pizza;
    }
}
