import pizza.Bakery;
import pizza.Pizza;
import pizza.PizzaType;
import pizzamaker.PizzaMaker;
import pizzamaker.PizzaMakerFactory;
import view.View;

public class Application {
    private View view;
    private ConsoleReader consoleReader;
    private PizzaMaker maker;
    Pizza pizza;

    public Application(View view) {
        this.view = view;
        this.consoleReader = new ConsoleReader(view);
    }

    public void runApplication() {
        Bakery bakery = chooseCity();
        PizzaType pizzaType = choosePizza();
        maker = PizzaMakerFactory.choosePizza(pizzaType);
        pizza = maker.createPizza(bakery);
        view.print(pizza.toString());

    }


    private Bakery chooseCity() {
        Bakery bakery;
        int city = consoleReader.readValue("Please, choose your city :\n 1 - " +
                Bakery.KYIV + "\n 2 - " + Bakery.LVIV + "\n 3 - " + Bakery.DNIPRO);
        if (city == 1) {
            bakery = Bakery.KYIV;
        } else if (city == 2) {
            bakery = Bakery.LVIV;
        } else if (city == 3) {
            bakery = Bakery.DNIPRO;
        } else {
            view.print("Unknown city, try one more time");
            bakery = chooseCity();
        }
        return bakery;
    }

    private PizzaType choosePizza() {
        PizzaType pizzaType;
        int city = consoleReader.readValue("Please, choose pizza :\n 1 - " + PizzaType.PEPPERONI + "\n 2 - "
                + PizzaType.CHEESE + "\n 3 - " + PizzaType.CLAM + "\n 4 - " + PizzaType.VEGGIE);
        if (city == 1) {
            pizzaType = PizzaType.PEPPERONI;
        } else if (city == 2) {
            pizzaType = PizzaType.CHEESE;
        } else if (city == 3) {
            pizzaType = PizzaType.CLAM;

        } else if (city == 4) {
            pizzaType = PizzaType.VEGGIE;
        } else {
            view.print("Unknown pizza, try one more time");
            pizzaType = choosePizza();
        }
        return pizzaType;

    }
}

