import view.View;

import java.util.Scanner;

public class ConsoleReader {
    private View view;

    public ConsoleReader(View view) {
        this.view = view;
    }

    public String scanFromConsole(String message) {
        Scanner scan = new Scanner(System.in);
        view.print(message);
        return scan.nextLine();
    }

    public int readValue(String message) {
        try {
            int result = Integer.parseInt(scanFromConsole(message));
            return result;
        } catch (NumberFormatException e) {
            view.print(" Please input digit value");
        }
        return readValue(message);
    }
}
